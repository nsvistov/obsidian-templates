function tagsCreate(tp, postfix = "todo") {
    let templateFileName = tp.config.template_file.basename;
    // Check that the template starts with and underscore
    if (templateFileName.charAt(0) !== '_') {
        // Filter the case of "Today's note" creation where these files are same for some reason O_o
        if (templateFileName !== tp.date.now()) {
            throw new Error("tagsCreate() called on template without an undescore in the begging of the filename: " + tp.config.template_file.basename);
        } else {
            templateFileName = "_journal_daily";
        }
    }

    const tagsArray = templateFileName.split("_");
    // Remove the part from the first underscore
    tagsArray.shift();

    // Append postfix unless unnecessary
    if (postfix !== "") {
        tagsArray.push(postfix);
    }

    return "tags: " + tagsArray.join("/");
}
module.exports = tagsCreate;