---
date: <% tp.file.creation_date() %>
<% tp.user.tagsCreate(tp, "idea") %>
code: ""
invested:
    Y<% tp.file.creation_date("YYYY") %>: 0
    review: <% tp.file.creation_date("YYYY-MM-DD") %>
---

# <% tp.file.title %>
Описание проекта/СС

## Целевая система
Описание ЦС

## Роли и рабочие продукты
| Роль | Рабочий продукт |
| ---- | --------------- |
|      |                 |

## Заметки
```dataview
LIST WITHOUT ID
FROM [[]] AND !#journal
```
## Задачи
```tasks
path includes <% tp.file.path(true) %>
not done
hide recurrence rule
hide backlink
```

## События

<% tp.file.include("[[_include_zero_links]]") %>