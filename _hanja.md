<%*
// prepare links
// the title has to include a whitespace
if ((tp.file.title.includes(" ")) == false) {
    return;
}

//  the title has to be odd (whitespace + 2x characters)
if (tp.file.title.length % 2 == 0) {
    return;
}

let hanjaLinks = "";

// split words
let hangeul = tp.file.title.split(" ")[0];
let hanja = tp.file.title.split(" ")[1];

// hangeul and hanja have to be same length
if (hangeul.length != hanja.length) {
    return;
}

// step by 2 default, if leftover is less, the substring() will take only one character
let jump = 2;
if (hangeul.length < 3) {
    // only in case of 2 characters go by 1
    jump = 1;
}

for(let i = 0; i < hangeul.length; i = i + jump) {
    hanjaLinks += "[[" + hangeul.substring(i, i + jump) + " "  + hanja.substring(i, i + jump) + "]] ";
}
hanjaLinks = hanjaLinks.slice(0, -1);

// do nothing for only 1 character
if (hangeul.length == 1) {
    hanjaLinks = "";
}
-%>
---
date: <% tp.file.creation_date() %>
<% tp.user.tagsCreate(tp, hangeul.length + "/todo") %>
---

# <% tp.file.title %><% tp.file.cursor() %>

<% tp.file.include("[[_include_zero_links]]") %><% hanjaLinks %>