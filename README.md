# Ilyas’ Obsidian.md Templates

Hi!

I'm using [Obsidian.md](https://obsidian.md) to take notes, generate content and ideas.

Here are my templates I wrote using the [Templater](https://silentvoid13.github.io/Templater/introduction.html) and the [Dataview](https://blacksmithgu.github.io/obsidian-dataview/) plugins. Feel free to use the templates at your risk ;)

If you have any ideas on how to make the same thing simpler, smarter, shorter, cleaner, please, don't hesitate and drop me a message =)

Thanks<br />
Ilyas
